# Documentation: Snapnet exercise
## Requirements

* Use flickr api or any api of your choice to get a list of photos (items) and display it on the page.
* Please paginate the list 10 photos per page.
* When the user clicks on the photo, show it in an overlay.

Note: Due to the simplicity of the exercise, I've decided to use a minimal build.

**Don't assume** that my skills are limited to these frameworks. I don't have much free time right now, so this is technically 45 min -> 1 hour of work total.

I have built enterprise applications using Typescript / Angular 1-4 / Ionic 2-3 / React -> React Native / and plenty more frameworks / languages I will leave mostly unnamed for the sake of this documentation. If you have any doubt about my skills based off this project due to the frameworks used -> please assign me a more complex task that is reasonable to use the frameworks desired.

I technically did not use a RESTful api, instead I scraped the response body using the node module request and gathered the images urls by loading them into the node module cheerio. If you need me to alter the code to use a RESTful api I will, but I do not like sending code with API keys and this was a simple enough exercise.


### Let's get started
#### Build:

* Node.js
	* express
	* request
	* cheerio
	* express-handlebars
* Gulp
* Sass / CSS3
* jQuery / JavaScript
* Handlebars / HTML5


To view this exercise online you can checkout the heroku app online -> https://snapnet.herokuapp.com

You will have to wait a couple seconds at first for the dyno to load on heroku due to this being a free hobby plan app.

To clone this repository run ```git clone https://bitbucket.org/iansawyerva/forbes.git```

To view this git repository use this url -> https://bitbucket.org/iansawyerva/forbes/src


You will need to have node and npm installed to test locally. Please reference https://nodejs.org/en/ for installation instructions.

You will also need gulp installed globally if you'd like to test the sass preprocessing.

```npm install -g gulp```

You might need to use ```sudo``` based on your permissions if you're on OSX or Linux.

Due to the simplicity of the exercise again, I have left out a .gitignore file, so you should not have to ```npm install``` when testing locally.

Once you have node and npm installed you will be able to run the project locally by running (in your terminal -> inside the root directory of the project) ```npm start``` or ```node index```


## Future
* Potential React / Angular Component?

## **Side notes**

### Skill checklists -> All list items are skills/ideologies I have.

### Technical Skills checklist

* Expert Knowledge of JavaScript, HTML5
* High level of Proficiency with TypeScript, JQuery, NodeJS
* Experience with one of the MV*/MVC frameworks: React / AngularJS
* Strong Knowledge of CSS3/ Compass / Sass / Adaptive and Responsive Implementations
* Experience with Git
* Experience with Grunt/Gulp automation
* Proficiency with Chrome Developer Tools
* Experience working with Rest APIs
* Experience with Web Components

### General Skills checklist

* Strong writing and communication skills for both technical and non-technical audiences -> I helped teach a coding bootcamp at GWU
* Ability to thrive in a fast-paced, deadline-driven environment
* Experience in being a mentor
* Create maintainable projects with documentation
* Experience with Git
* Experience with Grunt/**Gulp** automation
* Proficiency with Chrome Developer Tools
* Experience working with Rest APIs
* Experience with Web Components
* 5+ years front-end web development experience

### Desired Skills checklist

* Experience working with large-scale web applications
* Contributions to Open Source Projects
* Excited about digital media and publishing
* Passionate about the mobile web
* Genuine enthusiasm and strong sensibility for social publishing
* Experience working with PWAs
* Experience working with Service Workers

### There is plenty of unmentioned technologies that I have extensive experience using, such as Docker / AWS / PostgreSQL / SQL / RoR / Python &-> Django / etc