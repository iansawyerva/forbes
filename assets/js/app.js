$(function() {
  $('.overlay-close').on('click', function(e) {
    $('#OverlayComponent').removeClass('in');
  });
  $('.snap-image').on('click', function(e) {
    $('.overlay-img').remove();
    $('#OverlayComponent').append('<img src="' + $(this).find('img').attr("src") + '" class="overlay-img"/>')
    $('#OverlayComponent').addClass('in');
  });
  var snaps_length = $('.snap-image').length;
  var asyncAnimate = function(count, length, iteratorClass, addClass) {
    let reanimate = function() {
      return asyncAnimate(count, length, iteratorClass, addClass);
    };
    if (count < snaps_length) {
      $(iteratorClass + count).addClass(addClass);
      count++;
      setTimeout(reanimate, 125);
    }
  };
  asyncAnimate(0, snaps_length, '.snap-image', 'in');
});
