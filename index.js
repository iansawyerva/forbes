(function(key, app, http, request, cheerio, exphbs, express, path) {
  app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
  app.set('view engine', 'handlebars');
  app.disable('view cache');
  app.use('/static', express.static(path.join(__dirname, 'assets')))
  app.listen(process.env.PORT || 5000)

  function Snapnet(options) {
    self = this;
    this.load = function(url, cb, options) {
      request(url, function(error, response, body) {
        var $ = cheerio.load(body);
        cb($)
      });
    };
  }

  var html = new Snapnet();

  html.load('https://www.flickr.com/explore', function($) {
    var urls = [];
    var images = $('.photo-list-photo-view');
    images.each(function(index, img) {
      urls.push('https://' + img.attribs.style.match(/url((.*))/g)[0].replace('url(//', '').replace(')', ''));
    });
    var url_sets = [];
    var pagination = [];
    for (var i = 0; i < urls.length; i++) {
      if (urls.length) {
        url_sets.push(urls.splice(0, 10));
      } else {
        break;
      }
    }
    for (var i = 0; i < url_sets.length; i++) {
      pagination.push('/' + (i == 0 ? '' : i));
    }
    url_sets.forEach(function(url_set, index) {
      app.get('/' + (index == 0 ? '' : index), function(req, res) {
        res.render('snapnet', {
          urls: url_set,
          pages: pagination,
          current_page: '/' + (index == 0 ? '' : index)
        });
      });
    });
  });
})(Math.random().toString(36),
  require('express')(),
  require('http'),
  require('request'),
  require('cheerio'),
  require('express-handlebars'),
  require('express'),
  require('path'));

//write up compliance -> only for educational purposes, any commercial or enterprise productions should contact flickr before using these?
